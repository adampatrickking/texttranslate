﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace TextTranslate
{
    public partial class MainForm : Form
    {
        private bool _fullScreen = false;
             
        private Language _TranslateFrom { get; set; }
        private Language _TranslateTo { get; set; }
                
        public MainForm()
        {
            Translator._allDictionaries = new Dictionary<string, LanguageDictionary>();                      
            InitializeComponent();
        }

        /// <summary>
        /// A private event handler method called when the MainForm 
        /// instance loads, for operations that need to be performed 
        /// after the elements are available
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadDictionaries();

            // If there are no dictionaries, close the application
            if (Translator._allDictionaries.Values.Count < 1)
            {
                MessageBox.Show("No dictionary files were loaded, exiting...");
                Application.Exit();
            }

            // Use the Language Enum as names for the ComboBox values
            Language[] languages = (Language[]) Enum.GetValues(typeof(Language));

            foreach (Language language in languages)
            {
                // Add the value of language to the ComboBoxes
                translateFromCBox.Items.Add(language);
                translateToCBox.Items.Add(language);
            }
            
            // Set the default ComboBox values if they exist
            if (translateFromCBox.Items.Contains(Language.English))
                translateFromCBox.SelectedItem = Language.English;
            if (translateToCBox.Items.Contains(Language.Somalian))
                translateToCBox.SelectedItem = Language.Somalian;

            runSingleTranslateBtn.Enabled = true;
            resetFormBtn.Enabled = true;
        }
        
        /// <summary>
        /// Clears all of the dictionaries from the global array of LanguageDictionary,
        /// re-reading all of the dictionary files on disk, parsing the contents and 
        /// adding them to the global array of LanguageDictionary
        /// </summary>
        private void LoadDictionaries()
        {
            // Empty the ComboBox objects and the Dictionary of contents before loading
            Translator._allDictionaries.Clear();
            translateFromCBox.Items.Clear();
            translateToCBox.Items.Clear();

            Translator.AddDictionary("AllLanguage");
        }

        /// <summary>
        /// A private void event handler method to handle the changing of 
        /// the translateFromCBox ComboBox type on the form, checking whether
        /// it is already selected
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void translateFromCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Check if the SelectedItem property is the same as what 
            // we're setting, do nothing if this is the case
            if ((Language) translateFromCBox.SelectedItem != _TranslateFrom)
            {
                _TranslateFrom = (Language) translateFromCBox.SelectedItem;
                GetSetTranslateDisabled();
            }
        }

        /// <summary>
        /// A private void event handler method to handle the changing of 
        /// the translateToCBox ComboBox type on the form, checking whether
        /// it is already selected
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void translateToCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Check if the SelectedItem property is the same as what we're setting,
            // do nothing if this is the case
            if ((Language) translateToCBox.SelectedItem != _TranslateTo)
            {
                _TranslateTo = (Language) translateToCBox.SelectedItem;
                GetSetTranslateDisabled();
            }
        }

        /// <summary>
        /// Gets whether the translate buttons are disabled with an 
        /// optional boolean argument if you wish to set the state 
        /// to something different
        /// </summary>
        /// <param name="disable">A boolean to set the disabled status</param>
        /// <returns>True if buttons are disabled</returns>
        private bool GetSetTranslateDisabled(bool disable = false)
        {
            bool disabled = true;

            if (translateFromCBox.SelectedItem == null || translateToCBox.SelectedItem == null)
            {
                runTranslateBtn.Enabled = false;
                runSingleTranslateBtn.Enabled = false;
                disabled = true;
            }
            else if (translateFromCBox.SelectedItem != null && translateToCBox.SelectedItem != null)
            {
                runTranslateBtn.Enabled = true;
                runSingleTranslateBtn.Enabled = true;
                disabled = false;
            }

            return disabled;
        }

        /// <summary>
        /// A private void event handler method fired when the text is changed on 
        /// the input box to automatically translate text whenever a word is detected 
        /// as being terminated
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void inputRTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (inputRTxtBox.Text.Length > 0)
            {
                runTranslateBtn.PerformClick();
            }
            else
            {
                inputRTxtBox.Clear();
            }
        }

        /// <summary>
        /// A private void event handler method that re-loads the dictionaries
        /// and resets the form choices for language (ComboBox Type)
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void dictionaryReloadBtn_Click(object sender, EventArgs e)
        {
            // Use the LoadDictionaries method as it will empty the necessary 
            // contents of objects before re-reading the dictionary and repopulating
            LoadDictionaries();

            int entries = 0;

            foreach (LanguageDictionary dictionary in Translator._allDictionaries.Values)
            {
                entries += dictionary._Words.Count;
            }

            MessageBox.Show(string.Format("Loaded {0} words", entries));
        }
        
        /// <summary>
        /// A private void event handler method that empties form elements for any input/output
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void resetFormBtn_Click(object sender, EventArgs e)
        {
            // Empties the input and output text areas on the form
            inputRTxtBox.Clear();
            outputTxtBox.Clear();
        }

        /// Private void event handler method: Prefer Application.Exit() over the 
        /// Environment class method Exit() because it will handle any form 
        /// cleanup and errors/exceptions
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void exitApplicationBtn_Click(object sender, EventArgs e)
        {            
            Application.Exit();
        }

        /// <summary>
        /// Private void event handler method for the translate button; runs a 
        /// check on the dictionary for translations of words in the input box
        /// and displays the results when clicked
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void runTranslateBtn_Click(object sender, EventArgs e)
        {
            // Clear the text box before proceeding as we're using 
            // string concatenation here, not just setting
            outputTxtBox.Clear();

            Sentence[] sentences = Paragraph.Split(inputRTxtBox.Text, _TranslateFrom);
            Paragraph paragraph = new Paragraph(sentences);

            // Check for existing sentences, if there is already one in the text box (based on terminators),
            // then we should construct a paragraph instead of sentences 
            if (inputRTxtBox.Text.Length > 0 && Utils.ContainsTerminator(inputRTxtBox.Text))
            {
                outputTxtBox.Text = Translator.Translate(paragraph, _TranslateTo);
            }
            else if (inputRTxtBox.Text.Length > 0)
            {
                outputTxtBox.Text = string.Join(" ", Translator.Translate(sentences, _TranslateTo));
            }
        }

        /// <summary>
        /// Opens the single translation dialogue form, which accepts 
        /// one word and one language, and sets it to visible
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void runSingleTranslateBtn_Click(object sender, EventArgs e)
        {
            // An anonymous instance as the form will be very short lived.
            // The garbage collector will clear it as soon as it closes
            new FindWordForm(outputTxtBox).Visible = true;
        }

        /// <summary>
        /// A private event handler method for controlling the Exit button 
        /// on the window menu, it runs the exitApplicationBtn doing a 
        /// Button#PerformClick
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exitApplicationBtn.PerformClick();
        }

        /// <summary>
        /// A private event handler method for controlling the About button 
        /// on the window menu, it creates an anonymous instance of the 
        /// AboutForm class and sets it's Visible property to true
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create an anonymous instance and set
            // it's Visible property to true to display.
            // The garbage collector will dispose of it
            // once it is closed.            
            new About().Visible = true;
        }

        /// <summary>
        /// A private event handler method for the fullScreenBtn element,
        /// it sets the _fullScreen field and adjusts the window accordingly
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void fullScreenBtn_Click(object sender, EventArgs e)
        {
            if (!_fullScreen)
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                _fullScreen = true;
            }
            else
            {
                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
                _fullScreen = false;
            }
        }        
    }

    /// <summary>
    /// An Enum to use when passing language choices to methods so we 
    /// don't have to check if it is supported, avoid using strings
    /// </summary>
    public enum Language
    {
        English,
        Somalian,
        Spanish,
        French,
        Japanese,
        Russian
    }
}