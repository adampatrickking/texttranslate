﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

namespace TextTranslate
{
    //TODO:- Produce grammar methods which apply to multiple languages, best done in a non-programmatic format
    //       Otherwise there will be a lot of messy code which will make Grammar less flexible
    public sealed class GrammarParser
    {
        public Sentence Text { get; set; }
        public bool IsQuestion { get; set; }
        public bool IsExclamation { get; set; }

        public GrammarParser(Sentence sentence)
        {
            Text = sentence;

            IsQuestion = ParseQuestion();
            IsExclamation = ParseExclamation();
        }

        public GrammarParser(string text, Language language)
        {
            Text = new Sentence(text, language);
        }
                
        private bool ParseQuestion()
        {
            return (Text.Terminator == '?');
        }

        private bool ParseExclamation()
        {
            return (Text.Terminator == '!');
        }
    }
}