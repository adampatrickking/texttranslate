﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TextTranslate
{
    public static class Translator
    {
        public static Dictionary<string, LanguageDictionary> _allDictionaries = new Dictionary<string, LanguageDictionary>();

        /// <summary>
        /// A private void method to read a dictionary from file on local disk 
        /// and parse the words, populating it with translations and adding it 
        /// to the global array of LanguageDictionary 
        /// </summary>
        /// <param name="dictionaryName">The name of the dictionary to create/add</param>
        public static void AddDictionary(string dictionaryName)
        {
            // Create an instance of LanguageDictionary to push to _allDictionaries 
            LanguageDictionary dictionary = new LanguageDictionary(dictionaryName);

            // Add the language to the ComboBox if the LanguageDictionary is not empty
            if (dictionary._Name != null && dictionary._Words.Count > 0)
            {
                // Add to the _allDictionaries using the language 
                // variable as the key for each entry
                _allDictionaries.Add(dictionaryName, dictionary);
            }
        }

        /// <summary>
        /// A translate method that accepts one word, by default it removes the end punctuation 
        /// from the English word and concatenates it onto the end of the translated string for 
        /// a simple punctuation effect.
        /// </summary>
        /// <param name="word">The word to translate</param>
        /// <param name="translatedLanguage">The language to translate to</param>
        /// <param name="punctuation">Whether to include punctuation or not</param>
        /// <returns>Translated string</returns>
        public static string TranslateOne(string word, Language originalLanguage, Language translatedLanguage, bool punctuation = true)
        {
            string translation = null;
            
            char? punctuationChar = null;
            string parsedWord = null;

            if (word != null && word != "")
            {
                punctuationChar = Utils.GetTerminator(word);
                parsedWord = Utils.RemovePunctuation(word);

                foreach (LanguageDictionary dictionary in _allDictionaries.Values)
                {                    
                    string translated = dictionary.GetWord(parsedWord, translatedLanguage);

                    if (translated != null)
                    {
                        translation = translated;

                        if (punctuation)
                        {
                            translation += punctuationChar;
                        }
                    }
                }
            }

            return translation;
        }

        /// <summary>
        /// A translate method that accepts a long string of multiple words, splitting them by spaces 
        /// and translating them one by one, concatenating them onto the result to be returned
        /// </summary>
        /// <param name="words">The words to translate</param>
        /// <param name="language">The language to translate to</param>
        /// <param name="punctuation">Whether to include punctuation or not</param>
        /// <returns>Translated string</returns>
        public static string Translate(Paragraph paragraph, Language translatedlanguage, bool punctuation = true)
        {
            string translation = "";
            
            foreach (Sentence sentence in paragraph.Sentences)
            {
                // If the words contains a space then there must be more than one word
                if (sentence.Text.Length > 0 && sentence.Text.Contains(' '))
                {
                    foreach (string word in sentence.Text.Split(' '))
                    {
                        if (word != null && word.Length > 0)
                        {
                            // Use the return value of TranslateOne to concatenate the translated 
                            // string, adding and removing punctuation as necessary
                            translation += TranslateOne(word, sentence.Language, translatedlanguage, punctuation) + " ";
                        }
                    }
                }
                // If it's only one word (there isn't a space) then only run TranslateOne
                else if (sentence.Text != null && sentence.Text.Length > 0)
                {
                    // Use the return value of TranslateOne to concatenate the translated 
                    // string, adding and removing punctuation as necessary
                    translation += TranslateOne(sentence.Text, sentence.Language, translatedlanguage, punctuation) + " ";
                }
            }

            // Ternary return null if the length of the output is 0.
            // Returns the string of translated words otherwise
            return (translation.Length > 0) ? translation : null;
        }

        /// <summary>
        /// A translate method that accepts an array of strings to translate on by one, adding the 
        /// translations to a list and then return the List#ToArray
        /// </summary>
        /// <param name="words">The words to translate</param>
        /// <param name="translatedLanguage">The language to translate to</param>
        /// <param name="punctuation">Whether to include punctuation or not</param>
        /// <returns>Translated strings</returns>
        public static string[] Translate(Sentence[] sentences, Language translatedLanguage, bool punctuation = true)
        {
            List<string> translations = new List<string>();

            foreach (Sentence sentence in sentences)
            {
                foreach (string word in sentence.Words)
                {
                    if (word != null && word != "")
                    {
                        string translation = TranslateOne(word, sentence.Language, translatedLanguage, punctuation);

                        if (translation != null)
                        {
                            translations.Add(translation);
                        }
                    }
                }
            }

            return translations.ToArray();
        }
    }
}