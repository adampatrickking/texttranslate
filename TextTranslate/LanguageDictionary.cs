﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace TextTranslate
{
    public class LanguageDictionary
    {
        // This is essentially a multi-dimensional array with keys only
        // all entries at any level requires a key
        // Structure: Word -> language => WordAlternative
        public Dictionary<string, Dictionary<string, string>> _Words { get; set; }
        
        public string _Name { get; set; }
        private string _dictionaryFile;

        /// <summary>
        /// The default LanguageDictionary constructor, should perform minimal operations
        ///  and is expected that it's fields be set manually after initalization
        /// </summary>
        public LanguageDictionary()
        {
            _Words = new Dictionary<string, Dictionary<string, string>>();
        }

        /// <summary>
        /// An overload of the LanguageDictionary constructor to set the dictionary file name, 
        /// the name of the dictionary and load the dictionary from the file name
        /// </summary>
        /// <param name="dictionaryName">A string containing name of the dictionary file</param>
        public LanguageDictionary(string dictionaryName)
        {
            _Words = new Dictionary<string, Dictionary<string, string>>();

            this._dictionaryFile = "dictionaries/" + dictionaryName + ".dict";
            this._Name = dictionaryName;

            _Words = LoadDictionaryFromFile(_dictionaryFile);
        }

        /// <summary>
        /// A LanguageDictionary constructor to produce a named dictionary containing the 
        /// word->language->translatedWord structure
        /// </summary>
        /// <param name="name">Name of the dictionary</param>
        /// <param name="dictionary">The multidimensional dictionary</param>
        public LanguageDictionary(string name, Dictionary<string, Dictionary<string, string>> dictionary)
        {
            this._Name = name;
            this._Words = dictionary;
            this._dictionaryFile = null;
        }

        /// <summary>
        /// A method obtain the contents of the language file
        /// and add the contents one-by-one into a Dictionary
        /// </summary>
        /// <param name="fileName">A string containing the full file name of the dictionary</param>
        /// <returns>
        /// A multidimensional dictionary containing English words as keys,
        /// languages as sub-keys and translations as values
        /// </returns>
        public Dictionary<string, Dictionary<string, string>> LoadDictionaryFromFile(string fileName)
        {
            Dictionary<string, Dictionary<string, string>> languageDictionary = new Dictionary<string, Dictionary<string, string>>();
            
            try
            {
                if (File.Exists(fileName))
                {
                    // Obtain all of the lines from the file provided if it exists
                    string[] dictionaryContents = File.ReadAllLines(fileName, Encoding.ASCII);
                    
                    // Loop over the lines of the file and s
                    foreach (string entry in dictionaryContents)
                    {
                        int translations = Regex.Matches(entry, "{").Count;
                        string remainder = entry;
                        string wordTranslation;
                        string englishWord = null;
                        string key;
                        
                        // Loop over the translations for the word and get all
                        // of it's other possibilites 
                        for (int i = 0; i < translations; i++) 
                        {                            
                            // Set the indexes of the language name to use
                            // when adding to the full dictionary.
                            int keyStart = remainder.IndexOf('{') + 1;
                            int keyEnd = remainder.IndexOf('}', keyStart);
                            int keyLength = keyEnd - keyStart;
                            key = remainder.Substring(keyStart, keyLength);

                            // Set the indexes of the word start and finish to
                            // make life easier inside the inner loop
                            int wordStart = remainder.IndexOf('}') + 2;

                            int wordEnd = remainder.IndexOf('{', wordStart + 1) - 1;

                            // Check if the wordEnd is below zero as an 
                            // index using the startFrom argument that is 
                            // not found will return a negative integer
                            if (wordEnd < 0)
                            {
                                // If the wordEnd is not found then it must be the last
                                // entry in the line, treat it as such and set the index
                                // to the end of the line
                                wordEnd = remainder.Length;
                            }

                            int wordLength = wordEnd - wordStart;
                            wordTranslation = remainder.Substring(wordStart, wordLength).ToLower();
                                                        
                            // The first entry should always be English, to aid
                            // development, if it is we can obtain it to use as
                            // a key in the dictionary
                            if (i == 0)
                            {
                                Dictionary<string, string> translation = new Dictionary<string, string>();

                                englishWord = wordTranslation;

                                // Prematurely strip the current word from the remainder in
                                // case the loop has to continue (key already exists)
                                int substringFrom = remainder.IndexOf('{', wordEnd);
                                remainder = remainder.Substring(substringFrom, entry.Length - substringFrom);

                                // If the language dictionary does not already contain a 
                                // key->value pair for the current word then add it
                                if (!languageDictionary.ContainsKey(englishWord))
                                {
                                    translation.Add(key, englishWord);
                                    languageDictionary.Add(englishWord, translation);
                                }
                                else
                                {
                                    // If the dictionary already has a key for this
                                    // word then skip the rest of the languages on 
                                    // this line
                                    continue;
                                }
                            }
                            // If it's not the first index we can get the substring
                            // from the remainder string instead of the entry 
                            else
                            {
                                string translation = wordTranslation;

                                // Prematurely strip the current word from the remainder in
                                // case the loop has to continue (key already exists).
                                //
                                // Caution is taken here to test whether this operation is on 
                                // the last word in the remainder, otherwise substring will fail
                                // due to a bad index (IndexOf will return negative)
                                if (remainder.IndexOf('{', wordEnd) > 0)
                                {
                                    int substringFrom = remainder.IndexOf('{', wordEnd);
                                    remainder = remainder.Substring(substringFrom, remainder.Length - substringFrom);
                                }

                                // If there's an English word and the Dictionary does not already contain
                                // a value for that word 
                                if (englishWord != null && !languageDictionary[englishWord].ContainsValue(translation))
                                {
                                    languageDictionary[englishWord].Add(key, translation);
                                }
                            }
                        }
                    }                    
                }
                else
                {
                    throw new FileNotFoundException("Dictionary file not found.", fileName);
                }
            }
            catch (Exception e)
            {
                // Use System.Diagnostics to get the line number if possible
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(st.FrameCount - 1);
                var line = frame.GetFileLineNumber();
                
                // Silently print the exception message to the console for logging
                Console.WriteLine(string.Format("{0} on line number {1}", e.Message, line));

                // Check for the most expected exception here and output the missing file;
                // otherwise display the generic exception data
                if (e is FileNotFoundException)
                {
                    MessageBox.Show(string.Format("Dictionary file could not be found: {0}", fileName));
                }
                else
                {
                    MessageBox.Show(String.Format("Exception thrown during dictionary load from file: {0}. On line: {1}", e.Message, line));
                }
            }

            return languageDictionary;
        }

        /// <summary>
        /// A method to return from the private _words multi-dimensional dictionary 
        /// and handle errors/exceptions accordingly in a way that makes sense to the user
        /// </summary>
        /// <param name="englishWord">The word to translate, in English</param>
        /// <param name="language">The language to translate to as an Enum, Language</param>
        /// <returns>String containing the translated word if the value is found, null otherwise</returns>
        public string GetWord(string englishWord, Language language)
        {
            englishWord = englishWord.ToLower();

            try
            {
                string translation = this._Words[englishWord][language.ToString()];
                return translation;
            }
            catch (Exception e)
            {
                // For debugging only
                Console.WriteLine(e.Message);

                if (_Words.ContainsKey(englishWord))
                {
                    Console.WriteLine(string.Format("Translation not found for {0}: {1}", language.ToString(), englishWord));
                }
                else
                {
                    Console.WriteLine(string.Format("Word not found in the English dictionary: {0}", englishWord));
                }

                return null;
            }
        }
    }
}