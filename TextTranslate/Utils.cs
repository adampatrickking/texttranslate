﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TextTranslate
{
    public static class Utils
    {
        /// <summary>
        /// A static utility method to merge multiple dictionaries into one object, taking
        /// the largest dictionary and adding to that one to perform less operations in total
        /// </summary>
        /// <param name="dictionaryOne">A LanguageDictionary</param>
        /// <param name="dictionaryTwo">A LanguageDictionary</param>
        /// <returns>A LanguageDictionary with merged contents</returns>
        public static LanguageDictionary MergeDictionaries(LanguageDictionary dictionaryOne, LanguageDictionary dictionaryTwo)
        {
            // Local  to manipulate, set to null as not to waste memory we may not need;
            // (dictionariessaves the garbage collector the job of finding the resource and any relationships it may have)
            Dictionary<string, Dictionary<string, string>> mergedDictionary = null;
            Dictionary<string, Dictionary<string, string>> otherDictionary = null;
            string mergedDictionaryName = null;

            // At least one of the dictionaries must be populated, if both are empty 
            // there is no point in proceeding; return null
            if (dictionaryOne != dictionaryTwo && (dictionaryOne._Words.Count > 0 || dictionaryOne._Words.Count > 0))
            {
                // Initialize the dictionaries now since we can be sure we'll need the memory
                mergedDictionary = new Dictionary<string, Dictionary<string, string>>();
                otherDictionary = new Dictionary<string, Dictionary<string, string>>();

                // Loop over the dictionaries and compare them with .Contains()
                // Use the largest dictionary as the choice to add to.

                // Determine which dictionary is larger and use it as the mergedDictionary
                if (dictionaryOne._Words.Count > dictionaryTwo._Words.Count)
                {
                    mergedDictionary = dictionaryOne._Words;
                    otherDictionary = dictionaryTwo._Words;
                }
                else
                {
                    otherDictionary = dictionaryOne._Words;
                    mergedDictionary = dictionaryTwo._Words;
                }

                // Loop over the values of the otherDictionary to check whether they
                // exist at all in the mergedDictionary
                foreach (Dictionary<string, string> word in otherDictionary.Values)
                {
                    // The values will be strictly Language->Word and there are
                    // no root keys present; use the English key->value to act
                    // as the root key and then process the other language values                 
                    string key = word["english"];

                    if (!mergedDictionary.ContainsKey(key))
                    {
                        // Add the key and values of the other dictionary to the merged 
                        // dictionary using the fake top-level key produced from inner values
                        mergedDictionary.Add(key, otherDictionary[key]);
                    }
                }

                mergedDictionaryName = dictionaryOne._Name + dictionaryTwo._Name + "Merged";
            }

            return new LanguageDictionary(mergedDictionaryName, mergedDictionary);
        }

        /// <summary>
        /// A static utility method to remove punctuation from a word using Regular Expressions
        /// to match and replace groups of NOT words or spaces [^\w\s]
        /// </summary>
        /// <param name="input">The string to be stripped of punctuation</param>
        /// <returns>A string without characters/punctuation in it</returns>
        public static string RemovePunctuation(string input)
        {
            // Match a group [] of NOT ^ words \w or spaces \s
            return Regex.Replace(input, @"[^\w\s]", "");
        }

        /// <summary>
        /// A static utility method to retrieve the last character of a string if it is anything 
        /// other than a word or a space, returning it as a nullable char type in case nothing 
        /// else can be reliably returned
        /// </summary>
        /// <param name="input">The string to search for punctuation</param>
        /// <returns>The character of punctuation from the string</returns>
        public static char? GetTerminator(string input)
        {
            if (Regex.IsMatch(input, @"[^\w\s]"))
            {
                return input[input.Length - 1];
            }

            return null;
        }
        
        public static bool ContainsTerminator(string input)
        {
            return (input.IndexOfAny(Sentence.terminators.ToArray())) != -1;
        }
    }
}