﻿namespace TextTranslate
{
    partial class FindWordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputWordTxt = new System.Windows.Forms.TextBox();
            this.languageFromCBox = new System.Windows.Forms.ComboBox();
            this.findSingleWordBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.languageToCBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inputWordTxt
            // 
            this.inputWordTxt.Location = new System.Drawing.Point(13, 25);
            this.inputWordTxt.Name = "inputWordTxt";
            this.inputWordTxt.Size = new System.Drawing.Size(226, 20);
            this.inputWordTxt.TabIndex = 0;
            // 
            // languageFromCBox
            // 
            this.languageFromCBox.FormattingEnabled = true;
            this.languageFromCBox.Location = new System.Drawing.Point(13, 66);
            this.languageFromCBox.Name = "languageFromCBox";
            this.languageFromCBox.Size = new System.Drawing.Size(226, 21);
            this.languageFromCBox.TabIndex = 1;
            // 
            // findSingleWordBtn
            // 
            this.findSingleWordBtn.Location = new System.Drawing.Point(51, 150);
            this.findSingleWordBtn.Name = "findSingleWordBtn";
            this.findSingleWordBtn.Size = new System.Drawing.Size(148, 30);
            this.findSingleWordBtn.TabIndex = 2;
            this.findSingleWordBtn.Text = "Find Word";
            this.findSingleWordBtn.UseVisualStyleBackColor = true;
            this.findSingleWordBtn.Click += new System.EventHandler(this.findSingleWordBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Input Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Translate From";
            // 
            // languageToCBox
            // 
            this.languageToCBox.FormattingEnabled = true;
            this.languageToCBox.Location = new System.Drawing.Point(12, 110);
            this.languageToCBox.Name = "languageToCBox";
            this.languageToCBox.Size = new System.Drawing.Size(227, 21);
            this.languageToCBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Translate To";
            // 
            // FindWordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 192);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.languageToCBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.findSingleWordBtn);
            this.Controls.Add(this.languageFromCBox);
            this.Controls.Add(this.inputWordTxt);
            this.Name = "FindWordForm";
            this.Text = "Find Word";
            this.Load += new System.EventHandler(this.FindWordForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputWordTxt;
        private System.Windows.Forms.ComboBox languageFromCBox;
        private System.Windows.Forms.Button findSingleWordBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox languageToCBox;
        private System.Windows.Forms.Label label3;
    }
}