﻿using System;
using System.Windows.Forms;

namespace TextTranslate
{
    public partial class FindWordForm : Form
    {
        private TextBox Output { get; set; }

        /// <summary>
        /// A constructor to set the output of the translation
        /// </summary>
        /// <param name="output">TextBox to place the output in</param>
        public FindWordForm(TextBox output)
        {
            InitializeComponent();

            Output = output;
        }
        
        /// <summary>
        /// A private event handler method to access elements on the form
        /// once they have loaded; e.g. ComboBox contents
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void FindWordForm_Load(object sender, EventArgs e)
        {
            Language[] languages = (Language[]) Enum.GetValues(typeof(Language));

            foreach (Language language in languages)
            {
                // Add the value of language to the ComboBoxes
                languageFromCBox.Items.Add(language);
            }
        }

        /// <summary>
        /// A private event handler method to handle the translate button 
        /// on the FindWordForm, uses the Utils#TranslateOne method
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event Object</param>
        private void findSingleWordBtn_Click(object sender, EventArgs e)
        {
            Output.Clear();

            string translated = Translator.TranslateOne(inputWordTxt.Text, (Language) languageFromCBox.SelectedIndex, (Language) languageToCBox.SelectedItem, true);

            if (translated != null)
                Output.Text = translated;

            this.Close();
        }
    }
}
