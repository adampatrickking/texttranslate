﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

namespace TextTranslate
{
    public sealed class Paragraph
    {
        public Sentence[] Sentences { get; set; }
        public int Count { get; set; }
        public int WordCount { get; set; }
               
        public Paragraph(params Sentence[] sentences)
        {
            Sentences = sentences;
            Count = Sentences.Length;
                        
            foreach (Sentence sentence in Sentences)
            {
                WordCount += sentence.Count;
            }
        }

        /// <summary>
        /// Split a large body of text up into instances of the Sentence class which forms a paragraph. 
        /// This makes translation and grammar checking much more managed and easier to control.
        /// </summary>
        /// <param name="text">A string of text to be split</param>
        /// <param name="language">The Language of the text</param>
        /// <returns></returns>
        public static Sentence[] Split(string text, Language language)
        {
            List<Sentence> sentences = new List<Sentence>();
            
            // The amount of sentences (minus any improperly terminated ones)
            int terminators = text.Count(Sentence.terminators.Contains);

            for (int i = 0; i < terminators; i++)
            {
                // Find the end of the sentence
                int sentenceEnd = text.IndexOfAny(Sentence.terminators.ToArray());

                // Get the sentence before the terminator
                string sentence = text.Substring(0, sentenceEnd + 1);
                // Strip the sentence before the terminator
                text = text.Substring(sentenceEnd + 1);
                
                sentences.Add(new Sentence(sentence, language));
            }

            // If there are no sentence terminators left but there is still text in the sentence
            // we can grab it and apply a sentence terminator or add it without one
            if (text.Length > 0)
            {
                sentences.Add(new Sentence(text+=".", language));
            }

            // Add anything after the last terminator if the sentence isn't
            // terminated properly.

            return sentences.ToArray();
        }
    }
}