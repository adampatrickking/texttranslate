﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextTranslate
{
    public sealed class Sentence
    {
        public static List<char> terminators = new List<char>
        {
            '!',
            '?',
            '.'
        };

        public string Text { get; set; }
        public char Terminator { get; set; }
        public int Length { get; set; }
        public string[] Words { get; set; }
        public int Count { get; set; }
        public Language Language { get; set; }
        
        public Sentence(string text, Language language)
        {
            Text = text;
            Terminator = text[text.Length - 1];
            Length = text.Length;
            Words = text.Trim().Split(' ');
            Count = Words.Length;
            Language = language;
        }

        public GrammarParser ParseGrammar()
        {
            return new GrammarParser(this);
        }

        public override string ToString()
        {
            return Text;
        }
    }
}