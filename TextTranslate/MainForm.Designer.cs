﻿namespace TextTranslate
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dictionaryReloadBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.outputTxtBox = new System.Windows.Forms.TextBox();
            this.runTranslateBtn = new System.Windows.Forms.Button();
            this.resetFormBtn = new System.Windows.Forms.Button();
            this.exitApplicationBtn = new System.Windows.Forms.Button();
            this.runSingleTranslateBtn = new System.Windows.Forms.Button();
            this.translateFromCBox = new System.Windows.Forms.ComboBox();
            this.translateToCBox = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.inputRTxtBox = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.fullScreenBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dictionaryReloadBtn
            // 
            this.dictionaryReloadBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dictionaryReloadBtn.Location = new System.Drawing.Point(433, 43);
            this.dictionaryReloadBtn.Name = "dictionaryReloadBtn";
            this.dictionaryReloadBtn.Size = new System.Drawing.Size(106, 33);
            this.dictionaryReloadBtn.TabIndex = 1;
            this.dictionaryReloadBtn.Text = "Re-load Dictionary";
            this.dictionaryReloadBtn.UseVisualStyleBackColor = true;
            this.dictionaryReloadBtn.Click += new System.EventHandler(this.dictionaryReloadBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Input Text";
            // 
            // outputTxtBox
            // 
            this.outputTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputTxtBox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.outputTxtBox.Location = new System.Drawing.Point(3, 117);
            this.outputTxtBox.Multiline = true;
            this.outputTxtBox.Name = "outputTxtBox";
            this.outputTxtBox.ReadOnly = true;
            this.outputTxtBox.Size = new System.Drawing.Size(521, 108);
            this.outputTxtBox.TabIndex = 5;
            // 
            // runTranslateBtn
            // 
            this.runTranslateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.runTranslateBtn.Enabled = false;
            this.runTranslateBtn.Location = new System.Drawing.Point(178, 333);
            this.runTranslateBtn.Name = "runTranslateBtn";
            this.runTranslateBtn.Size = new System.Drawing.Size(171, 31);
            this.runTranslateBtn.TabIndex = 6;
            this.runTranslateBtn.Text = "Translate";
            this.runTranslateBtn.UseVisualStyleBackColor = true;
            this.runTranslateBtn.Click += new System.EventHandler(this.runTranslateBtn_Click);
            // 
            // resetFormBtn
            // 
            this.resetFormBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resetFormBtn.Enabled = false;
            this.resetFormBtn.Location = new System.Drawing.Point(15, 333);
            this.resetFormBtn.Name = "resetFormBtn";
            this.resetFormBtn.Size = new System.Drawing.Size(63, 31);
            this.resetFormBtn.TabIndex = 7;
            this.resetFormBtn.Text = "Reset";
            this.resetFormBtn.UseVisualStyleBackColor = true;
            this.resetFormBtn.Click += new System.EventHandler(this.resetFormBtn_Click);
            // 
            // exitApplicationBtn
            // 
            this.exitApplicationBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exitApplicationBtn.Location = new System.Drawing.Point(485, 333);
            this.exitApplicationBtn.Name = "exitApplicationBtn";
            this.exitApplicationBtn.Size = new System.Drawing.Size(54, 31);
            this.exitApplicationBtn.TabIndex = 8;
            this.exitApplicationBtn.Text = "Exit";
            this.exitApplicationBtn.UseVisualStyleBackColor = true;
            this.exitApplicationBtn.Click += new System.EventHandler(this.exitApplicationBtn_Click);
            // 
            // runSingleTranslateBtn
            // 
            this.runSingleTranslateBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.runSingleTranslateBtn.Enabled = false;
            this.runSingleTranslateBtn.Location = new System.Drawing.Point(355, 333);
            this.runSingleTranslateBtn.Name = "runSingleTranslateBtn";
            this.runSingleTranslateBtn.Size = new System.Drawing.Size(124, 31);
            this.runSingleTranslateBtn.TabIndex = 9;
            this.runSingleTranslateBtn.Text = "Find Single Word";
            this.runSingleTranslateBtn.UseVisualStyleBackColor = true;
            this.runSingleTranslateBtn.Click += new System.EventHandler(this.runSingleTranslateBtn_Click);
            // 
            // translateFromCBox
            // 
            this.translateFromCBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.translateFromCBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.translateFromCBox.FormattingEnabled = true;
            this.translateFromCBox.Location = new System.Drawing.Point(3, 8);
            this.translateFromCBox.Name = "translateFromCBox";
            this.translateFromCBox.Size = new System.Drawing.Size(201, 21);
            this.translateFromCBox.TabIndex = 10;
            this.translateFromCBox.SelectedIndexChanged += new System.EventHandler(this.translateFromCBox_SelectedIndexChanged);
            // 
            // translateToCBox
            // 
            this.translateToCBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.translateToCBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.translateToCBox.FormattingEnabled = true;
            this.translateToCBox.Location = new System.Drawing.Point(210, 8);
            this.translateToCBox.Name = "translateToCBox";
            this.translateToCBox.Size = new System.Drawing.Size(202, 21);
            this.translateToCBox.TabIndex = 11;
            this.translateToCBox.SelectedIndexChanged += new System.EventHandler(this.translateToCBox_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(551, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.helpToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem1.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Translate From";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Translate To";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.translateToCBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.translateFromCBox, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 43);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(415, 37);
            this.tableLayoutPanel1.TabIndex = 15;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.outputTxtBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.inputRTxtBox, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 99);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(527, 228);
            this.tableLayoutPanel2.TabIndex = 16;
            // 
            // inputRTxtBox
            // 
            this.inputRTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputRTxtBox.Location = new System.Drawing.Point(3, 3);
            this.inputRTxtBox.Name = "inputRTxtBox";
            this.inputRTxtBox.Size = new System.Drawing.Size(521, 108);
            this.inputRTxtBox.TabIndex = 6;
            this.inputRTxtBox.Text = "";
            this.inputRTxtBox.TextChanged += new System.EventHandler(this.inputRTxtBox_TextChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label4, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(12, 29);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(415, 16);
            this.tableLayoutPanel3.TabIndex = 17;
            // 
            // fullScreenBtn
            // 
            this.fullScreenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.fullScreenBtn.Location = new System.Drawing.Point(84, 334);
            this.fullScreenBtn.Name = "fullScreenBtn";
            this.fullScreenBtn.Size = new System.Drawing.Size(88, 30);
            this.fullScreenBtn.TabIndex = 18;
            this.fullScreenBtn.Text = "Fullscreen";
            this.fullScreenBtn.UseVisualStyleBackColor = true;
            this.fullScreenBtn.Click += new System.EventHandler(this.fullScreenBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(551, 374);
            this.Controls.Add(this.fullScreenBtn);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.runSingleTranslateBtn);
            this.Controls.Add(this.exitApplicationBtn);
            this.Controls.Add(this.resetFormBtn);
            this.Controls.Add(this.runTranslateBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dictionaryReloadBtn);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(567, 412);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SBCC Simple Translate";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button dictionaryReloadBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox outputTxtBox;
        private System.Windows.Forms.Button runTranslateBtn;
        private System.Windows.Forms.Button resetFormBtn;
        private System.Windows.Forms.Button exitApplicationBtn;
        private System.Windows.Forms.Button runSingleTranslateBtn;
        private System.Windows.Forms.ComboBox translateFromCBox;
        private System.Windows.Forms.ComboBox translateToCBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RichTextBox inputRTxtBox;
        private System.Windows.Forms.Button fullScreenBtn;
    }
}

